﻿using Android.Views;
using Fragment = AndroidX.Fragment.App.Fragment;

namespace GNow_Net.Fragments
{
    public class DrawerFragment : Fragment
    {
        public DrawerFragment() { }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Inflate the layout for this fragment
            return inflater.Inflate(Resource.Layout.DrawerFragment, container, false);
        }
    }
}