﻿using Android.Views;
using Android.Webkit;
using Uri = Android.Net.Uri;

namespace GNow_Net
{
    public class WebClient : WebViewClient
    {
        private readonly ProgressBar indeterminateBar;
        private readonly WebView webView;
        public WebClient(WebView webView)
        {
            this.webView = webView;
        }

        public WebClient(ProgressBar indeterminateBar, WebView webView)
        {
            this.indeterminateBar = indeterminateBar;
            this.webView = webView;
        }

        public override void OnPageFinished(WebView view, string url)
        {
            if (indeterminateBar != null) 
            {
                indeterminateBar.Visibility = ViewStates.Gone;
            }
            webView.Visibility = ViewStates.Visible;
        }

        [System.Obsolete]
        public override bool ShouldOverrideUrlLoading(WebView view, string url)
        {
            return !Uri.Parse(url).Host.Contains("fanmixco.github.io");
        }

        public override bool ShouldOverrideUrlLoading(WebView view, IWebResourceRequest request)
        {
            return !request.Url.ToString().Contains("fanmixco.github.io");
        }
    }
}