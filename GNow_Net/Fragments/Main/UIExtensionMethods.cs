﻿using Google.Android.Material.TextField;
using System.Threading.Tasks;

namespace GNow_Net
{
    internal static class UIExtensionMethodsHelpers
    {
        public static async Task<bool> GetIdle(this TextInputEditText txb)
        {
            string txt = txb.Text;
            await Task.Delay(500);
            return txt == txb.Text;
        }
    }
}