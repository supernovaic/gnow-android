﻿using System.Collections.Generic;
using AndroidX.Fragment.App;
using AndroidX.ViewPager2.Adapter;
using Fragment = AndroidX.Fragment.App.Fragment;

namespace GNow_Net.Adapters
{
    class MainAdapter(FragmentActivity fragmentActivity) : FragmentStateAdapter(fragmentActivity)
	{
        private readonly List<Fragment> fragments =
            [
                HomeFragment.NewInstance(),
                CalcFragment.NewInstance(),
                ComparisonFragment.NewInstance(),
                OSMSearchFragment.NewInstance(),
                GNowPreview.NewInstance()
            ];

        public override int ItemCount
        {
            get { return fragments.Count; }
        }

        public override Fragment CreateFragment(int p0)
        {
            return fragments[p0];
        }
	}
}