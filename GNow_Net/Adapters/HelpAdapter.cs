﻿using System.Collections.Generic;
using AndroidX.Fragment.App;
using AndroidX.ViewPager2.Adapter;
using Fragment = AndroidX.Fragment.App.Fragment;

namespace GNow_Net.Adapters
{
	class HelpAdapter(FragmentActivity fragmentActivity) : FragmentStateAdapter(fragmentActivity)
	{
		private readonly List<Fragment> fragments =
            [
                DefinitionFragment.NewInstance(),
                GravityCFragment.NewInstance(),
                FormulaFragment.NewInstance()
            ];

        public override int ItemCount
		{
			get { return fragments.Count; }
		}

		public override Fragment CreateFragment(int p0)
        {
			return fragments[p0];
		}
	}
}