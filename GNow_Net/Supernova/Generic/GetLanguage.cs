﻿using System.Globalization;

namespace Supernova.Model.Generic
{
    class GetLanguage
    {
        public static string GetCurrentLanguage()
        {
            if (!new List<string>() { "en", "es" }.Contains(CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToLower()))
            {
                return "en";
            }

            return CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToLower();
        }
    }
}