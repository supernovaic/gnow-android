﻿namespace Supernova.Enums
{
    public enum Units
    {
        Meters,
        Feet
    }
}