using Android.Graphics.Drawables;

namespace Supernova.Model.Generic
{
    public class DrawerData
    {
        public string name { get; set; }
        public Drawable image { get; set; }
        public string color { get; set; }
        public string url { get; set; }
    }
}