﻿namespace Supernova.Model.Generic
{
    public class CelestialDataLight
    {
        public CelestialDataLight()
        {

        }

        public CelestialDataLight(string name, string type, string gravity, string comparison)
        {
            Name = name;
            Type = type;
            Gravity = gravity;
            Comparison = comparison;
        }

        public string Name { get; set; }
        public string Type { get; set; }
        public string Gravity { get; set; }
        public string Comparison { get; set; }
    }
}