﻿using Android.Content;
using Java.IO;
using SQLite;
using Supernova.Database.DAO;
using Supernova.Database.Entities;
using Supernova.Enums;

namespace Supernova.Database
{
    public class DatabaseManagement
    {
        private const string DB_NAME = "gnow.db";

        protected static SQLiteAsyncConnection ConnectionDb()
        {
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var conn = new SQLiteAsyncConnection(Path.Combine(folder, DB_NAME), true);
            return conn;
        }

        public static void CopyDataBase(Context context, bool recreate = false)
        {
            try
            {
                var file = new Java.IO.File(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), DB_NAME));

                if (!file.Exists() || recreate)
                {
                    if (recreate)
                    {
                        file.Delete();
                    }

                    if (!file.ParentFile.Exists())
                    {
                        file.ParentFile.Mkdirs(); // Create parent directories
                    }

                    file.CreateNewFile();

                    using Stream istream = context.Assets.Open(DB_NAME);
                    using var outDB = new FileOutputStream(file);
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = istream.Read(buffer)) > 0)
                    {
                        outDB.Write(buffer, 0, length);
                    }

                    // Flush and close the output stream
                    outDB.Flush();
                    outDB.Close();
                }
            }
            catch (Java.IO.IOException e)
            {
                e.PrintStackTrace();
            }
        }

        public async static Task<Profile> AddProfile()
        {
            ProfileDAO.Insert(new Profile() { Email = "", Sync = false, CalcDecimal = 8, SelectedUnits = Units.Meters, NDecimal = 3, Name = "" });
            return await new ProfileDAO().GetProfile();
        }
    }
}