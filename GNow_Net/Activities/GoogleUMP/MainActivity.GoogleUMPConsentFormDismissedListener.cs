﻿namespace GNow_Net
{
    public partial class MainActivity
    {
        public class GoogleUMPConsentFormDismissedListener : Java.Lang.Object, Xamarin.Google.UserMesssagingPlatform.IConsentFormOnConsentFormDismissedListener
        {
            public GoogleUMPConsentFormDismissedListener(Action<Xamarin.Google.UserMesssagingPlatform.FormError> failureAction)
            {
                a = failureAction;
            }
            public void OnConsentFormDismissed(Xamarin.Google.UserMesssagingPlatform.FormError f)
            {
                a(f);
            }

            private Action<Xamarin.Google.UserMesssagingPlatform.FormError> a = null;
        }
    }
}