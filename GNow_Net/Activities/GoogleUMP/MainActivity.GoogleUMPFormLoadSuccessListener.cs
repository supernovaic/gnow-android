﻿namespace GNow_Net
{
    public partial class MainActivity
    {
        public class GoogleUMPFormLoadSuccessListener : Java.Lang.Object, Xamarin.Google.UserMesssagingPlatform.UserMessagingPlatform.IOnConsentFormLoadSuccessListener
        {
            public GoogleUMPFormLoadSuccessListener(Action<Xamarin.Google.UserMesssagingPlatform.IConsentForm> successAction)
            {
                a = successAction;
            }
            public void OnConsentFormLoadSuccess(Xamarin.Google.UserMesssagingPlatform.IConsentForm f)
            {
                a(f);
            }

            private Action<Xamarin.Google.UserMesssagingPlatform.IConsentForm> a = null;
        }
    }
}