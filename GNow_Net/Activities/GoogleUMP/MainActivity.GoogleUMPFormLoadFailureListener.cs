﻿namespace GNow_Net
{
    public partial class MainActivity
    {
        public class GoogleUMPFormLoadFailureListener : Java.Lang.Object, Xamarin.Google.UserMesssagingPlatform.UserMessagingPlatform.IOnConsentFormLoadFailureListener
        {
            public GoogleUMPFormLoadFailureListener(Action<Xamarin.Google.UserMesssagingPlatform.FormError> failureAction)
            {
                a = failureAction;
            }
            public void OnConsentFormLoadFailure(Xamarin.Google.UserMesssagingPlatform.FormError e)
            {
                a(e);
            }

            private Action<Xamarin.Google.UserMesssagingPlatform.FormError> a = null;
        }
    }
}