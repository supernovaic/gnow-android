﻿namespace GNow_Net
{
    public partial class MainActivity
    {
        public class GoogleUMPConsentUpdateFailureListener : Java.Lang.Object, Xamarin.Google.UserMesssagingPlatform.IConsentInformationOnConsentInfoUpdateFailureListener
        {
            public GoogleUMPConsentUpdateFailureListener(Action<Xamarin.Google.UserMesssagingPlatform.FormError> failureAction)
            {
                a = failureAction;
            }
            public void OnConsentInfoUpdateFailure(Xamarin.Google.UserMesssagingPlatform.FormError f)
            {
                a(f);
            }

            private Action<Xamarin.Google.UserMesssagingPlatform.FormError> a = null;
        }
    }
}