﻿namespace GNow_Net
{
    public partial class MainActivity
    {
        public class GoogleUMPConsentUpdateSuccessListener : Java.Lang.Object, Xamarin.Google.UserMesssagingPlatform.IConsentInformationOnConsentInfoUpdateSuccessListener
        {
            public GoogleUMPConsentUpdateSuccessListener(Action successAction)
            {
                a = successAction;
            }

            public void OnConsentInfoUpdateSuccess()
            {
                a();
            }

            private Action a = null;
        }
    }
}