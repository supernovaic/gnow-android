using Android.Content;
using Android.Gms.Ads;
using Android.Views;
using Android.Views.TextClassifiers;
using AndroidX.AppCompat.App;
using AndroidX.Core.View;
using AndroidX.DrawerLayout.Widget;
using AndroidX.Preference;
using AndroidX.ViewPager2.Widget;
using AuditApp.Android;
using Com.Github.Florent37.Bubbletab;
using GNow_Net.Adapters;
using Google.Android.Material.NavigationRail;
using Microsoft.Maui.ApplicationModel;
using Supernova.Database.DAO;
using Supernova.Database.Entities;
using Supernova.Enums;
using Supernova.Model.Generic;
using static Android.Widget.AdapterView;
using ActionBar = AndroidX.AppCompat.App.ActionBar;
using AlertDialog = AndroidX.AppCompat.App.AlertDialog;

namespace GNow_Net
{
    [Android.App.Activity(Label = "Gravity Now!", MainLauncher = true, Icon = "@drawable/ic_icon")]
    public partial class MainActivity : AppCompatActivity
    {
        private const string NASA_URL = "https://2014.spaceappschallenge.org/awards/#globalawards";
        private Xamarin.Google.UserMesssagingPlatform.IConsentForm googleUMPConsentForm = null;
        private Xamarin.Google.UserMesssagingPlatform.IConsentInformation googleUMPConsentInformation = null;

        public DrawerLayout MDrawerLayout { get; set; }
        public ListView MDrawerList { get; set; }
        public TextView TxtCurrentSeekHome { get; set; }
        public TextView TxtCurrentSeekCalc { get; set; }
        public TextView LblOurEmail { get; set; }
        public ImageView ImgSpaceApps { get; set; }
        public Switch BtnDisableLocation { get; set; }
        public SeekBar SeekBarHome { get; set; }
        public SeekBar SeekBarCalc { get; set; }
        public Spinner SpinnerUnits { get; set; }
        public AlertDialog ProfileDialog { get; set; }
        public Profile Profile { get; set; }
        public ActionBar MainActionBar { get; set; }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetTheme(Resource.Style.Theme_GNowTheme_Material);

            SetContentView(Resource.Layout.Main);

            CheckDB();

            MobileAds.Initialize(ApplicationContext);

            SetGDPR();

            FindViewById<AdView>(Resource.Id.adView).LoadAd(new AdRequest.Builder().Build());

#if DEBUG
            FindViewById<AdView>(Resource.Id.adView).Visibility = ViewStates.Gone;
#endif

            MDrawerList = FindViewById<ListView>(Resource.Id.navList);
            MDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            MDrawerLayout.AddDrawerListener(new MainActionBarDrawerToggle(this, MDrawerLayout, Resource.String.ApplicationName, Resource.String.ApplicationName));

            MainActionBar = SupportActionBar;

            MainActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            MainActionBar.SetDisplayHomeAsUpEnabled(true);

            if (Resources.Configuration.Orientation == Android.Content.Res.Orientation.Portrait)
            {
                var bubbleTab = FindViewById<BubbleTab>(Resource.Id.bubbleTab);
                var viewPager = FindViewById<ViewPager2>(Resource.Id.viewPager);

                viewPager.Adapter = new MainAdapter(this);

                viewPager.RegisterOnPageChangeCallback(new OnPageChangeCallBack(viewPager));

                bubbleTab.SetupWithViewPager(viewPager);
            }
            else
            {
                var sideTab = FindViewById<NavigationRailView>(Resource.Id.main_navigation);

                sideTab.ItemSelected += SideTab_ItemSelected;

                sideTab.SelectedItemId = Resource.Id.menu_home;
            }

            AddDrawerItems();

            Rating();

            Platform.Init(this, bundle);
        }

        private void SetGDPR()
        {
            Console.WriteLine("DEBUG: MainActivity.OnCreate: Starting consent management flow, via UserMessagingPlatform.");
            try
            {
#if DEBUG
                    var debugSettings = new Xamarin.Google.UserMesssagingPlatform.ConsentDebugSettings.Builder(this)
                    .SetDebugGeography(Xamarin.Google.UserMesssagingPlatform.ConsentDebugSettings
                            .DebugGeography
                            .DebugGeographyEea)
                    .AddTestDeviceHashedId(Android.Provider.Settings.Secure.GetString(this.ContentResolver,
                                                        Android.Provider.Settings.Secure.AndroidId))
                    .Build();
#endif

                var requestParameters = new Xamarin.Google.UserMesssagingPlatform.ConsentRequestParameters
                    .Builder()
                    .SetTagForUnderAgeOfConsent(false)
#if DEBUG
                        .SetConsentDebugSettings(debugSettings)
#endif
                        .Build();

                var consentInformation = Xamarin.Google.UserMesssagingPlatform.UserMessagingPlatform.GetConsentInformation(this);

                consentInformation.RequestConsentInfoUpdate(
                    this,
                    requestParameters,
                    new GoogleUMPConsentUpdateSuccessListener(
                        () =>
                        {
                            // The consent information state was updated.
                            // You are now ready to check if a form is available.
                            if (consentInformation.IsConsentFormAvailable)
                            {
                                Xamarin.Google.UserMesssagingPlatform.UserMessagingPlatform.LoadConsentForm(
                                    this,
                                    new GoogleUMPFormLoadSuccessListener((Xamarin.Google.UserMesssagingPlatform.IConsentForm f) => {
                                        googleUMPConsentForm = f;
                                        googleUMPConsentInformation = consentInformation;
                                        Console.WriteLine("DEBUG: MainActivity.OnCreate: Consent management flow: LoadConsentForm has loaded a form, which will be shown if necessary, once the ViewModel is ready.");
                                        DisplayAdvertisingConsentFormIfNecessary();
                                    }),
                                    new GoogleUMPFormLoadFailureListener((Xamarin.Google.UserMesssagingPlatform.FormError e) => {
                                        // Handle the error.
                                        Console.WriteLine("ERROR: MainActivity.OnCreate: Consent management flow: failed in LoadConsentForm with error " + e.Message);
                                    }));
                            }
                            else
                            {
                                Console.WriteLine("DEBUG: MainActivity.OnCreate: Consent management flow: RequestConsentInfoUpdate succeeded but no consent form was available.");
                            }
                        }),
                    new GoogleUMPConsentUpdateFailureListener(
                        (Xamarin.Google.UserMesssagingPlatform.FormError e) =>
                        {
                            // Handle the error.
                            Console.WriteLine("ERROR: MainActivity.OnCreate: Consent management flow: failed in RequestConsentInfoUpdate with error " + e.Message);
                        })
                    );
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: MainActivity.OnCreate: Exception thrown during consent management flow: ", ex);
            }
        }
        public void DisplayAdvertisingConsentFormIfNecessary()
        {
            try
            {
                if (googleUMPConsentForm != null && googleUMPConsentInformation != null)
                {
                    /* ConsentStatus:
                        Unknown = 0,
                        NotRequired = 1,
                        Required = 2,
                        Obtained = 3
                    */
                    if (googleUMPConsentInformation.ConsentStatus == 2)
                    {
                        Console.WriteLine("DEBUG: MainActivity.DisplayAdvertisingConsentFormIfNecessary: Consent form is being displayed.");
                        DisplayAdvertisingConsentForm();
                    }
                    else
                    {
                        Console.WriteLine("DEBUG: MainActivity.DisplayAdvertisingConsentFormIfNecessary: Consent form is not being displayed because consent status is " + googleUMPConsentInformation.ConsentStatus.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("ERROR: MainActivity.DisplayAdvertisingConsentFormIfNecessary: consent form or consent information missing.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: MainActivity.DisplayAdvertisingConsentFormIfNecessary: Exception thrown: ", ex);
            }
        }

        public void DisplayAdvertisingConsentForm()
        {
            try
            {
                if (googleUMPConsentForm != null && googleUMPConsentInformation != null)
                {
                    Console.WriteLine("DEBUG: MainActivity.DisplayAdvertisingConsentForm: Consent form is being displayed.");

                    googleUMPConsentForm.Show(this, new GoogleUMPConsentFormDismissedListener(
                            (Xamarin.Google.UserMesssagingPlatform.FormError f) =>
                            {
                                if (googleUMPConsentInformation.ConsentStatus == 2) // required
                                {
                                    Console.WriteLine("ERROR: MainActivity.DisplayAdvertisingConsentForm: Consent was dismissed; showing it again because consent is still required.");
                                    DisplayAdvertisingConsentForm();
                                }
                            }));
                }
                else
                {
                    Console.WriteLine("ERROR: MainActivity.DisplayAdvertisingConsentForm: consent form or consent information missing.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: MainActivity.DisplayAdvertisingConsentForm: Exception thrown: ", ex);
            }
        }

        private void SideTab_ItemSelected(object sender, Google.Android.Material.Navigation.NavigationBarView.ItemSelectedEventArgs e)
        {
            LoadFragment(e.P0.ItemId);
        }

        private void LoadFragment(int id)
        {
            AndroidX.Fragment.App.Fragment fragment = null;
            switch (id)
            {
                case Resource.Id.menu_home:
                    fragment = HomeFragment.NewInstance();
                    break;
                case Resource.Id.menu_calc:
                    fragment = CalcFragment.NewInstance();
                    break;
                case Resource.Id.menu_compare:
                    fragment = ComparisonFragment.NewInstance();
                    break;
                case Resource.Id.menu_search:
                    fragment = OSMSearchFragment.NewInstance();
                    break;
                case Resource.Id.menu_map:
                    fragment = GNowPreview.NewInstance();
                    break;
            }

            if (fragment == null)
            {
                return;
            }

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame_data, fragment)
                .Commit();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.main_menu, menu);

            return true;
        }

        private void ImgSpaceApps_Click(object sender, EventArgs e)
        {
            var builder = new AndroidX.Browser.CustomTabs.CustomTabsIntent.Builder();

            var customTabsIntent = builder.Build();

            customTabsIntent.LaunchUrl(this, Android.Net.Uri.Parse(NASA_URL));
        }

        private void Rating()
        {
            try
            {
                AndroidPlaystoreAudit.Instance.TimeUntilPrompt = new TimeSpan(0, 0, 10);
                AndroidPlaystoreAudit.Instance.UsesUntilPrompt = 3;
                AndroidPlaystoreAudit.Instance.PromptTitle = GetString(Resource.String.LblTitleReview);
                AndroidPlaystoreAudit.Instance.ReviewAppStoreButtonText = GetString(Resource.String.LblReviewApp);
                AndroidPlaystoreAudit.Instance.DontRemindButtonText = GetString(Resource.String.LblDontReview);
                AndroidPlaystoreAudit.Instance.RemindLaterButtonText = GetString(Resource.String.LblRemindMeOneWeek);
                AndroidPlaystoreAudit.Instance.RemindLaterTimeToWait = new TimeSpan(168, 0, 0);

                AndroidPlaystoreAudit.Instance.AppStoreNotFound += (sender, e) =>
                {
                    Toast.MakeText(this, GetString(Resource.String.noStore), ToastLength.Long).Show();
                };
                AndroidPlaystoreAudit.Instance.Run(this);
            }
            catch { }
        }

        public override void OnBackPressed()
        {
            if (MDrawerLayout.IsDrawerOpen(GravityCompat.Start))
            {
                MDrawerLayout.CloseDrawers();
            }
            else
            {
                base.OnBackPressed();
            }
        }

        private async void CheckDB()
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(this);

            var firstTime = prefs.GetString("FirstRun", "");

            if (string.IsNullOrEmpty(firstTime))
            {
                Supernova.Database.DatabaseManagement.CopyDataBase(this);
                Profile = await Supernova.Database.DatabaseManagement.AddProfile();

                ISharedPreferencesEditor editor = prefs.Edit();
                editor.PutString("FirstRun", DateTime.Now.ToString("yyyy-MMM-dd"));
                editor.Apply();
            }
            else
            {
                Profile = await new ProfileDAO().GetProfile();
            }
        }

        private void AddDrawerItems()
        {
            MDrawerList.Adapter = new DrawerAdapter(new List<DrawerData>
            {
                new DrawerData() { name = GetString(Resource.String.Settings), image = GetDrawable(Resource.Drawable.cog) },
                new DrawerData() { name = GetString(Resource.String.WhatIsGraivity), image = GetDrawable(Resource.Drawable.earth) },
                new DrawerData() { name = GetString(Resource.String.LblFiveStars), image = GetDrawable(Resource.Drawable.star) },
                new DrawerData() { name = GetString(Resource.String.LblOtherApps), image = GetDrawable(Resource.Drawable.apps) },
                new DrawerData() { name = GetString(Resource.String.LblMyBooks), image = GetDrawable(Resource.Drawable.bookshelf) },
                new DrawerData() { name = GetString(Resource.String.AboutUs), image = GetDrawable(Resource.Drawable.information) }
            }); ;

            MDrawerList.ItemClick += (object sender, ItemClickEventArgs e) =>
            {
                switch (e.Position)
                {
                    case 0:
                        CreateSettingsDialog();
                        ProfileDialog.Show();
                        break;
                    case 1:
                        StartActivity(new Intent(this, typeof(Help)));
                        break;
                    case 2:
                        DisplayMarketStore();
                        break;
                    case 3:
                        OpenURL("https://play.google.com/store/apps/dev?id=5690548735972645731");
                        break;
                    case 4:
                        var bookURL = GetLanguage.GetCurrentLanguage() != "es" ? "https://a.co/d/3omQXkZ" : "https://a.co/d/hnu4Orp";
                        break;
                    case 5:
                        ShowAboutUs();
                        break;
                }

                CheckDrawerStatus();
            };
        }

        private void OpenURL(string url)
        {
            StartActivity(new Intent(Intent.ActionView, Android.Net.Uri.Parse(url)));
        }

        private void ShowAboutUs()
        {
            using View dialogView = base.LayoutInflater.Inflate(Resource.Layout.About, null);
            var dialog = new AlertDialog.Builder(this, Resource.Style.Theme_GNowAlertTheme);
            dialog.SetView(dialogView);
            dialog.SetTitle(Resource.String.AboutUs);
            ImgSpaceApps = dialogView.FindViewById<ImageView>(Resource.Id.imgSpaceApps);
            LblOurEmail = dialogView.FindViewById<TextView>(Resource.Id.lblOurEmail);
            LblOurEmail.MovementMethod = Android.Text.Method.LinkMovementMethod.Instance;
            ImgSpaceApps.Click += ImgSpaceApps_Click;
            dialog.Show();
        }

        private void DisplayMarketStore()
        {
            try
            {
                OpenURL($"market://details?id={PackageName}");
            }
            catch (ActivityNotFoundException)
            {
                Toast.MakeText(this, GetString(Resource.String.noStore), ToastLength.Long).Show();
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void CreateSettingsDialog()
        {
            using View dialogView = base.LayoutInflater.Inflate(Resource.Layout.Settings, null);
            var dialog = new AlertDialog.Builder(this, Resource.Style.Theme_GNowAlertTheme_Material);

            dialog.SetView(dialogView);
            BtnDisableLocation = dialogView.FindViewById<Switch>(Resource.Id.btnDisableLocation);
            SeekBarHome = dialogView.FindViewById<SeekBar>(Resource.Id.seekBarHome);
            SeekBarCalc = dialogView.FindViewById<SeekBar>(Resource.Id.seekBarCalc);
            SpinnerUnits = dialogView.FindViewById<Spinner>(Resource.Id.spinnerUnits);
            TxtCurrentSeekHome = dialogView.FindViewById<TextView>(Resource.Id.txtCurrentSeekHome);
            TxtCurrentSeekCalc = dialogView.FindViewById<TextView>(Resource.Id.txtCurrentSeekCalc);

            SeekBarHome.ProgressChanged += SeekBarHome_ProgressChanged;
            SeekBarCalc.ProgressChanged += SeekBarCalc_ProgressChanged;

            SeekBarCalc.Progress = Profile.CalcDecimal;
            SeekBarHome.Progress = Profile.NDecimal;

            TxtCurrentSeekHome.Text = Profile.NDecimal.ToString();
            TxtCurrentSeekCalc.Text = Profile.CalcDecimal.ToString();

            var adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerItem, new List<string>
                {
                    GetString(Resource.String.hintUnitsMetric),
                    GetString(Resource.String.hintUnitsEmperial)
                });

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);

            SpinnerUnits.Adapter = adapter;
            SpinnerUnits.SetSelection((int)Profile.SelectedUnits);

            BtnDisableLocation.Checked = Profile.Sync;

            dialog.SetTitle(Resource.String.Settings);

            dialog.SetPositiveButton(Resource.String.btnSave, BtnSave_Click);
            dialog.SetNegativeButton(Resource.String.btnCancel, (sender, e) => { });

            ProfileDialog = dialog.Create();
        }

        private void SeekBarCalc_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            TxtCurrentSeekCalc.Text = SeekBarCalc.Progress.ToString();
        }

        private void SeekBarHome_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            TxtCurrentSeekHome.Text = SeekBarHome.Progress.ToString();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            Profile.CalcDecimal = SeekBarCalc.Progress;
            Profile.NDecimal = SeekBarHome.Progress;
            Profile.Sync = BtnDisableLocation.Checked;
            Profile.SelectedUnits = (Units)SpinnerUnits.SelectedItemPosition;

            ProfileDAO.Update(Profile);

            Toast.MakeText(this, GetString(Resource.String.lblSave), ToastLength.Short).Show();

            ProfileDialog.Dismiss();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    if (MDrawerLayout.IsDrawerOpen(GravityCompat.Start))
                    {
                        MDrawerLayout.CloseDrawers();
                    }
                    else
                    {
                        MDrawerLayout.OpenDrawer(GravityCompat.Start);
                    }

                    return true;
                case Resource.Id.action_help:
                    HelpModal();
                    return true;
                default:
                    return false;
            }
        }

        private void HelpModal()
        {
            View dialogView = base.LayoutInflater.Inflate(Resource.Layout.HelpFormulaModal, null);
            using var Dialog = new Android.App.AlertDialog.Builder(this);
            Dialog.SetView(dialogView);
            Dialog.Show();
        }

        private void CheckDrawerStatus()
        {
            if (MDrawerLayout.IsDrawerOpen(GravityCompat.Start))
            {
                MainActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
                MDrawerLayout.CloseDrawers();
            }
        }
    }
}